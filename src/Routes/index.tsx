import React from "react";
import { Switch, Route } from "react-router-dom";
import CreateBuilding from "../components/create-building";
import UpdateBuilding from "../components/update-building";
import Map from "../components/map"

export default function Routes() {
    return (
        <Switch>
            <Route exact={true} path="/" >
                <h1>Home</h1>
            </Route>
            <Route path="/create-building">
                <CreateBuilding />
            </Route>
            <Route  path={`/update-building/:id`}>
                <UpdateBuilding />
            </Route>
            <Route  path={`/buildings/:id`}>
                <Map />
            </Route>
        </Switch>
    );
}
