import Header from '../components/Header';
import SideBar from '../components/SideBar';
import Routes from '../Routes/';
import Toast from '../components/Toast';
import context from '../context/context';
import React, {useContext} from "react";

function Layout() {
    const contextIns = useContext(context);
    const position = "top-right";
    const toastList = [contextIns.notification];

    React.useEffect(() => {
        const buildings = JSON.parse(localStorage.getItem("buildings"));
        contextIns.setBuildings(buildings || []);
      }, []);

    return (
        <div className="container">
            {contextIns.notification?.title && (
                <Toast position={position} toastList={toastList} />
            )}
            <Header />
            <SideBar />
            <main className="content">
                <Routes />
            </main>
        </div>
    )
}

export default Layout;