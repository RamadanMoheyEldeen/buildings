import React, { useContext } from 'react';
import BuildingForm from './building-form';
import {Country} from '../Models/Country';
import context from '../context/context';
import { useHistory } from 'react-router-dom';
import useToast from '../hooks/useToast';

const UpdateBuilding = () => {
    const contextIns = useContext(context);
    const building = contextIns.building;
    const contextInst = useContext(context);
    const history = useHistory();
    const toast = useToast();
    const update = (name: string, country: Country, id: number) =>{
        contextInst.updateBuilding({name, country, id});
        toast("success", `Building named "${name}" updated successfully`);   
        history.push(`/buildings/${id}`);
    }

    return (
        <>
            <h1>
                Update Building
            </h1>
            <BuildingForm isUpdate={true} handleAction={update} building={building}/>
        </>
    )
}

export default UpdateBuilding;