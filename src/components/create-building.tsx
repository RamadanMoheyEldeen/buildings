import React, {useContext} from 'react';
import BuildingForm from './building-form';
import { Country } from '../Models/Country';
import context from '../context/context';
import { useHistory } from 'react-router-dom';
import useToast from '../hooks/useToast';

const CreateBuilding = () => {
    const history = useHistory();
    const toast = useToast();
    const contextInst = useContext(context);
    const onCreate = (name: string, country: Country) =>{
        const id = new Date().valueOf();
        contextInst.createBuilding({name, country, id});
        toast("success", `Building named "${name}" create successfully`);   
        history.push(`/buildings/${id}`);
    }

    return (
        <>
            <h1>
                Create Building
            </h1>
            <BuildingForm isUpdate={false} handleAction={onCreate} />
        </>
    )
}

export default CreateBuilding;