import React, { useContext, useEffect, useState } from 'react'
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import { useParams } from 'react-router-dom';
import context from '../context/context';

const containerStyle = {
    width: '100%',
    height: 'calc(100vh - 150px)'
};

const Map = () => {
    const contextInst = useContext(context);
    const params = useParams();
    const building = contextInst.buildings.find(i => i.id == params.id);
    const [isShowen, showInfo] = useState(false);
    const [center, setCenter] = useState({
        lat: 28.212732317073176,
        lng: 31.432993658536578
    });
    useEffect(() => {
        if (building?.name) {
            setCenter({
                lat: Number(building.country.position[0]),
                lng: Number(building.country.position[1])
            }
            )
        }
    }, [building])

    const handleMouseOver = () => {
        showInfo(true)
    }
    const handleMouseExit = () => {
        showInfo(false)
    }
    return (
        <div className='mapContainer'>
            <LoadScript
                googleMapsApiKey="AIzaSyBoOa4mo-qBfTZS4rX4cTglbi0refuj3cQ"
            >
                <GoogleMap
                    mapContainerStyle={containerStyle}
                    center={center}
                    zoom={5}
                >
                    <Marker 
                        position={center}
                        onMouseOver={handleMouseOver} 
                        onMouseOut={handleMouseExit}
                     >
                        {isShowen && (
                            <InfoWindow>
                                <p>{`${building.name} Located in ${building.country.name}`}</p>
                            </InfoWindow>
                        )}
                    </Marker>
                </GoogleMap>
            </LoadScript>
        </div>
    )
}

export default Map;