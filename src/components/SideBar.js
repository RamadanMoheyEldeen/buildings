

import React, { useContext } from 'react';
import styles from '../styles/SideBar.module.css';
import { useHistory } from "react-router-dom";
import context from '../context/context'
import useToast from '../hooks/useToast';

function SideBar() {
    const contextInst = useContext(context);
    const { buildings } = contextInst;
    const history = useHistory();
    const toast = useToast();
    
    const openMap = (item) => {
        history.push(`/`);
        history.push(`/buildings/${item.id}`);
    }

    const updateBuilding = (e, id) => {
        e.stopPropagation()
        const building = contextInst.buildings.find(i=>i.id === id)
        contextInst.setSelectedBuilding(building);
        history.push(`/update-building/${id}`);
    }

    const deleteBuilding = (e, id) => {
        e.stopPropagation()
        contextInst.deleteBuilding(id);
        toast("success", `Building deleted successfully`);   
        history.push(`/`);
    }


    return (<div className={styles.sidebar}>
        <div onClick={()=>history.push('/')} className={styles.title}>
            Buildings
        </div>
        <ul className={styles.list}>
            {
                buildings.map(item => {
                    return (
                        <li onClick={() => openMap(item)} key={item.id} className={styles.listItem}>
                            <span>{item.name}</span>
                            <div className={styles.icons}>
                                <img onClick={(e)=> updateBuilding(e,item.id)} className={styles.icon} src="/icons/edit.svg" alt="edit" />
                                <img onClick={(e)=> deleteBuilding(e,item.id)} className={styles.icon} src="/icons/delete.svg" alt="delete" />
                            </div>
                        </li>
                    )
                })
            }
        </ul>
        <button onClick={() => history.push('/create-building')} className={styles.addButton}>Add Building</button>
    </div>
    )
}

export default SideBar;
