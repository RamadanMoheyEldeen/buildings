import React, { useState, ReactElement, useEffect, useContext } from 'react';
import styles from '../styles/BuildingForm.module.css';
import countries from '../countriesList.json';
import { Building } from '../Models/Building';
import { useHistory } from 'react-router-dom';

function BuildingForm(props: { isUpdate?: Boolean, handleAction: Function, building?: Building }, context?: any): ReactElement<any, any> | null {

    const [country, setSelectedCountry] = useState('');
    const [buildingName, changeBuildingName] = useState('');
    const [isShowen, showMessage] = useState(false);
    const history = useHistory();
    useEffect(() => {
        if (props.building?.name) {
            changeBuildingName(props.building?.name)
            handleChangeCountry(props.building?.country?.name)
        }
    }, [props.building?.country?.name, props.building?.name])

    const handleChangeCountry = (event: any) => {
        if (event?.target)
            setSelectedCountry(event.target.value)
        else
            setSelectedCountry(event);
    }

    const showValidationMessage = () => {
        showMessage(true);
        setTimeout(() => {
            showMessage(false);
        }, 5000)
    }

    const handleClick = () => {
        if (!country || !buildingName) {
            showValidationMessage()
            return;
        }
        if (!props.isUpdate) {
            props.handleAction(buildingName, countries.find(c => c.name === country));
        } else {
            props.handleAction(buildingName, countries.find(c => c.name === country), props.building?.id)
        }
    }

    return (
        <>
            <div className={styles.inputForm}>
                <div>
                    Building Name
                </div>
                <div>
                    <input
                        type="text"
                        value={buildingName}
                        onChange={(e) => changeBuildingName(e.target.value)}
                        name="building-name"
                    />
                </div>
            </div>
            <div className={styles.inputForm}>
                <div>
                    Location
                </div>
                <div>
                    <select defaultValue="" value={country} onChange={handleChangeCountry}>
                        <option disabled value="">
                            Select Country
                        </option>
                        {
                            countries.map((item) => {
                                return (
                                    <option value={item.name} key={item.id}  >
                                        {item.name}
                                    </option>
                                )
                            })
                        }
                    </select>
                </div>
            </div>
            {isShowen && <div className='danger'>
                Building Name and location is required.
            </div>}
            <div className="buttonsContainer">
                <button onClick={() => history.push('/')} className="cancel">
                    Cancel
                </button>
                <button onClick={handleClick} className='new'>
                    {props.isUpdate ? 'Update' : 'Create'}
                </button>
            </div>
        </>
    )
}

export default BuildingForm;