import React from 'react';
import styles from '../styles/Header.module.css';

const Header = () => (
    <header className={styles.header}>
        <img className={styles.image} src="/icons/logo.png" alt="logo" />
    </header>
)

export default Header;