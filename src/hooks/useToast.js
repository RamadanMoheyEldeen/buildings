import { useContext } from "react";
import context from "../context/context";

function useToast() {
    const contextIns = useContext(context);

    function notify(type, message) {
        switch (type) {
            case "success":
                contextIns.setNotification({
                    title: "Success",
                    description: message,
                    backgroundColor: "#5cb85c",
                });
                setTimeout(() => {
                    contextIns.setNotification({
                        title: "",
                        description: "",
                        backgroundColor: "",
                    });
                }, 5000);
                break;
            case "danger":
                contextIns.setNotification({
                    title: "Danger",
                    description: message,
                    backgroundColor: "#d9534f",
                });
                setTimeout(() => {
                    contextIns.setNotification({
                        title: "",
                        description: "",
                        backgroundColor: "",
                    });
                }, 5000);
                break;
            case "info":
                contextIns.setNotification({
                    title: "Info",
                    description: message,
                    backgroundColor: "#5bc0de",
                });
                setTimeout(() => {
                    contextIns.setNotification({
                        title: "",
                        description: "",
                        backgroundColor: "",
                    });
                }, 5000);
                break;
            case "warnning":
                contextIns.setNotification({
                    title: "Warning",
                    description: message,
                    backgroundColor: "#f0ad4e",
                });
                setTimeout(() => {
                    contextIns.setNotification({
                        title: "",
                        description: "",
                        backgroundColor: "",
                    });
                }, 5000);
                break;
            default:
                return {};
        }
    }
    return notify;
}

export default useToast;
