
export interface Messages {
    title: string;
    description: string;
    backgroundColor: string;
}