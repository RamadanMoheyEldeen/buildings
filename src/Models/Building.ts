import { Country } from './Country';

export interface Building {
    name: string;
    country: Country;
    id: number
}