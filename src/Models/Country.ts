export interface Country {
        id: string;
        name: string;
        position: Array<Number>;
}