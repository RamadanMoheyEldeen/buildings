import React from "react";
import Layout from "./Layouts/";
import State from './context/State';
import './styles/globals.css'
import {  BrowserRouter as Router, } from "react-router-dom";


function App() {
  return (
    <State>
      <Router>
       <Layout/>
      </Router>
    </State>
  );
}

export default App;
