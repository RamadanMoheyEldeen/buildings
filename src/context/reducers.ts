import { BuildingsState } from './context';
import { Building } from '../Models/Building';
import { Messages } from '../Models/Messages'

export const CREATE_BUILDING = "CREATE_BUILDING";
export const UPDATE_BUILDING = "UPDATE_BUILDING";
export const SET_SELECTED_BUILDING = "SET_SELECTED_BUILDING";
export const DELETE_BUILDING = "DELETE_BUILDING";
export const SET_NOTIFICATION = "SET_NOTIFICATION";
export const SET_BUILDINGS = "SET_BUILDINGS";

interface CreateBuildingAction {
  type: 'CREATE_BUILDING';
  building: Building;
}

interface UpdateBuildingAction {
  type: 'UPDATE_BUILDING';
  building: Building;
}

interface SetSelectedBuilding {
  type: 'SET_SELECTED_BUILDING';
  building: Building;
}

interface DeleteBuilding {
  type: "DELETE_BUILDING";
  id: number;
}

interface SetNotification {
  type: "SET_NOTIFICATION",
  notification: Messages
}

interface SetBuildings {
  type: "SET_BUILDINGS",
  buildings: Array<Building>
}

export type Actions = CreateBuildingAction | UpdateBuildingAction | SetSelectedBuilding | DeleteBuilding | SetNotification | SetBuildings;

const createBuilding = (building: Building, state: BuildingsState) => {
  return { ...state, buildings: [...state.buildings, building] };
};

const updateBuilding = (building: Building, state: BuildingsState) => {
  const index = state.buildings.findIndex(build => build.id === building.id);
  const buildings = [...state.buildings];
  buildings[index] = building;
  return { ...state, buildings };
};

const setSelectedBuilding = (building: Building, state: BuildingsState) => {
  return { ...state, building };
}

const deleteBuilding = (id: number, state: BuildingsState) => {
  return { ...state, buildings: state.buildings.filter(i => i.id !== id) };
}

const setNotification = (notification: Messages, state: BuildingsState) => {
  return { ...state, notification };
};

const setBuildings = (buildings: Array<Building>, state: BuildingsState) => {
  return {...state, buildings}
}

export const Reducer = (state: BuildingsState, action: Actions) => {
  switch (action.type) {
    case "CREATE_BUILDING":
      return createBuilding(action.building, state);
    case "UPDATE_BUILDING":
      return updateBuilding(action.building, state);
    case "SET_SELECTED_BUILDING":
      return setSelectedBuilding(action.building, state);
    case "DELETE_BUILDING":
      return deleteBuilding(action.id, state);
    case "SET_NOTIFICATION":
      return setNotification(action.notification, state);
    case "SET_BUILDINGS":
        return setBuildings(action.buildings, state);
    default:
      const _exhaust: never = action;
      return state;
  }

};
