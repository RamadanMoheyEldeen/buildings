import { Building } from './../Models/Building';
import { Messages } from './../Models/Messages'
import React from "react";

export interface BuildingsState{
    buildings: Array<Building>;
    building: any;
    notification: any
}

export interface BuildingsActions {
    createBuilding: (building: Building) => unknown;
    updateBuilding: (building: Building) => unknown;
    setSelectedBuilding: (building: Building) => unknown;
    deleteBuilding: (id: number) => unknown;
    setNotification: (notification: Messages) =>  unknown
    setBuildings: (buildings: Array<Building>) => unknown
}

export default React.createContext<BuildingsState & BuildingsActions>({
  buildings: [],
  building: {},
  notification: {},
  createBuilding: (building) => {},
  updateBuilding: (building) => {},
  setSelectedBuilding: (building) => {},
  deleteBuilding: (building) => {},
  setNotification: (notification) => {}, 
  setBuildings: (buildings) => {}
});
