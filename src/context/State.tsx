import React, { useReducer, ReactNode, ReactElement } from "react";
import MyAppContext from "./context";
import {
  Reducer,
  CREATE_BUILDING,
  UPDATE_BUILDING,
  SET_SELECTED_BUILDING,
  DELETE_BUILDING,
  SET_NOTIFICATION,
  SET_BUILDINGS
} from "./reducers";
import {Building} from '../Models/Building'
import {Messages} from '../Models/Messages'



function GlobalState(props: { children?: ReactNode; }, context?: any): ReactElement<any, any> | null {
  const [state, dispatch] = useReducer(Reducer, {
    buildings: [],
    building: {},
    notification: {}
  });

  const createBuilding = (building: Building) => {
    dispatch({ type: CREATE_BUILDING, building });
  };

  const updateBuilding = (building: Building) => {
    dispatch({ type: UPDATE_BUILDING, building });
  };

  const setSelectedBuilding = (building: Building) => {
    dispatch({ type: SET_SELECTED_BUILDING, building})
  }

  const deleteBuilding = (id: number) => {
    dispatch({ type: DELETE_BUILDING, id})
  }

  const setNotification = (notification: Messages ) => {
    dispatch({type: SET_NOTIFICATION, notification})
  }

  const setBuildings = (buildings: Array<Building>) => {
    dispatch({type: SET_BUILDINGS, buildings})
  }
  
  React.useEffect(() => {
    localStorage.setItem('buildings', JSON.stringify(state.buildings))
  }, [state.buildings])

  return (
    <MyAppContext.Provider
      value={{
        buildings: state.buildings,
        building: state.building,
        notification: state.notification,
        createBuilding: createBuilding,
        updateBuilding: updateBuilding,
        setSelectedBuilding: setSelectedBuilding,
        deleteBuilding: deleteBuilding,
        setNotification: setNotification,
        setBuildings: setBuildings
      }}
    >
      {props.children}
    </MyAppContext.Provider>
  );
};

export default GlobalState;
